#!/bin/bash

# This script lets you switch between desktop and dev hosts for each of this services -  sapiinquiry, e1phomeoffers, e1pautooffers, sapioffers.
# Usage ./e1p-change-env.sh sapiinquiry=desktop sapioffers=desktop e1pautooffers=desktop e1phomeoffers=desktop
# or ./e1p-change-env.sh sapiinquiry=dev sapioffers=dev e1pautooffers=dev e1phomeoffers=dev
# or ./e1p-change-env.sh sapiinquiry=desktop sapioffers=dev e1pautooffers=desktop e1phomeoffers=dev
# or ./e1p-change-env.sh sapiinquiry=dev sapioffers=desktop 
# ... or any combination you want.
# the script currently updates applications listed in the applicationPaths variable

applicationPaths=("e1p-comparative-rater sapi-offers sapi-transfer")

basepath=$P20_HOME
if [ ! -d "$basepath" ]; then
    echo "$basepath does not exist. Did you set the P20_HOME environment variable correctly?" 
    exit 1
fi

pathSuffix="/src/main/resources/application-desktop.properties"

http="http:\/\/"
devserver="qa-enterpriseservices.homesitep2.com"

sapiinquiry_desktop="localhost:8096"
sapiinquiry_dev=$devserver
sapiinquiry_hosts=("sapi.inquiry.api.host  inquiry.host")

sapioffers_desktop="localhost:8098"
sapioffers_dev=$devserver
sapioffers_hosts="sapi.offers.api.host"

e1phomeoffers_desktop="localhost:8083"
e1phomeoffers_dev=$devserver
e1phomeoffers_hosts="e1p.home.offers.host"

e1pautooffers_desktop="localhost:8092"
e1pautooffers_dev=$devserver
e1pautooffers_hosts="e1p.auto.offers.host"

backupConfigFile() {
    if [[ ! -f $1.orig ]]; then
        echo "Backing up $1"
        cp $1 $1.orig
    fi
}

updateConfigFiles () {
    app_env=$1_$2
    app_hosts=$1_hosts

    for path in ${applicationPaths[@]}; do # loop over all the applications that need their configs updated
        #backupConfigFile $basepath$path$pathSuffix

        for appHost in ${!app_hosts} ; do # the application-desktop.properties files don't refer to hosts in the same way, some call inquiries inquiry.host, other call it sapi.inquiry.api.host 
            sed  -i "" "s/$appHost.*/$appHost=$http${!app_env}/" $basepath$path$pathSuffix 
        done
    done
}

for ARGUMENT in "$@"
do
    app=$(echo $ARGUMENT | cut -f1 -d=)
    env=$(echo $ARGUMENT | cut -f2 -d=)   

    case "$app" in
            sapiinquiry | e1phomeoffers | e1pautooffers | sapioffers)      
                updateConfigFiles $app $env
                ;;
            *) 
            echo "$app not recognized" 
            ;;   
    esac    
done