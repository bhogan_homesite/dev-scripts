#!/bin/bash

basepath=$P20_HOME
if [ ! -d "$basepath" ]; then
    echo "$basepath does not exist. Did you set the P20_HOME environment variable correctly?" 
    exit 1
fi

applications=("e1p-comparative-rater sapi-e1p-auto-offers sapi-e1p-home-offers sapi-e1p-transfer sapi-inquiry sapi-offers sapi-transfer")

pullResults=()


for application in ${applications[@]}; do
    pullOutcome="Failure"

    echo "Pulling $application"
    git -C $basepath$application pull
    if [ $? == 0 ]
    then 
        pullOutcome="Success"
    fi
    echo  ""
    pullResults+=("$application, $pullOutcome")
done

echo -e "\n\n\n"
for pullResult in "${pullResults[@]}"
do
    echo $pullResult
done
