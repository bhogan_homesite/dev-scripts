#!/bin/bash

basepath=$P20_HOME
if [ ! -d "$basepath" ]; then
    echo "$basepath does not exist. Did you set the P20_HOME environment variable correctly?" 
    exit 1
fi

applications=("e1p-comparative-rater sapi-e1p-auto-offers sapi-e1p-home-offers sapi-e1p-transfer sapi-inquiry sapi-offers sapi-transfer")

for application in ${applications[@]}; do
    echo "Checking $application"
    cd $basepath$application
    git checkout dev 
    echo ""
    echo ""
done
