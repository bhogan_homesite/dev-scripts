#!/bin/bash

basepath=$P20_HOME
if [ ! -d "$basepath" ]; then
    echo "$basepath does not exist. Did you set the P20_HOME environment variable correctly?" 
    exit 1
fi

# all
applications=("sapi-e1p-auto-offers sapi-e1p-home-offers sapi-e1p-transfer sapi-inquiry sapi-offers sapi-transfer e1p-comparative-rater")

# not e1p-comparative-rater
#applications=("sapi-e1p-auto-offers sapi-e1p-home-offers sapi-e1p-transfer sapi-inquiry sapi-offers sapi-transfer")

# not sapi-e1p-home-offers
#applications=("sapi-e1p-auto-offers sapi-e1p-transfer sapi-inquiry sapi-offers sapi-transfer e1p-comparative-rater")

##### Use java -jar to start the application ######
###################################################
function runjava() {
    for application in ${applications[@]}; do
        if [ "$application" = "sapi-inquiry" ]
        then
            jar=$(ls -t $basepath$application/target/inquiry*.jar | head -1)
            osascript -e 'tell app "Terminal"
                do script "java -Dspring.profiles.active=desktop -server -jar '"$jar"' "
            end tell'
        elif [ "$application" = "sapi-offers" ]
        then
        jar=$(ls -tr $basepath$application/target/offers*.jar | head -1)
            osascript -e 'tell app "Terminal"
                do script "java -Dspring.profiles.active=desktop -server -jar '"$jar"' "
            end tell'
        elif [ "$application" = "e1p-comparative-rater" ]
        then
        jar=$(ls -tr $basepath$application/target/e1p-comprater*.jar | head -1)
            osascript -e 'tell app "Terminal"
                do script "java -Dspring.profiles.active=desktop -server -jar '"$jar"' "
            end tell'
        else 
            jar=$(ls -t $basepath$application/target/$application*.jar | head -1)
            osascript -e 'tell app "Terminal"
                do script "java -Dspring.profiles.active=desktop -server -jar '"$jar"' "
             end tell'
        fi

    done
}

##### Use mvn to start the applications #####
############################################
function runmvn() {
    for application in ${applications[@]}; do
    if [ "$application" != "e1p-comparative-rater" ]
    then
        osascript -e 'tell app "Terminal"
            do script "mvn -f '"$basepath"''"$application"'/pom.xml -Dorg.slf4j.simpleLogger.defaultLogLevel=WARN spring-boot:run -Dspring-boot.run.profiles=desktop " 
        end tell'
    elif [ "$application" = "e1p-comparative-rater" ]
    then
        osascript -e 'tell app "Terminal"
            do script "mvn -f '"$basepath"''"e1p-comparative-rater/parent-pom"'/pom.xml -Dorg.slf4j.simpleLogger.defaultLogLevel=WARN spring-boot:run -Dspring-boot.run.profiles=desktop " 
        end tell'
    fi
    done


}

# # you can do it this way too, explicitly writing the mvn or java jar command.
# #osascript -e 'tell app "Terminal"
#     # do script "mvn -f /Users/bhogan/dev/p20/sapi-e1p-auto-offers/pom.xml -Dorg.slf4j.simpleLogger.defaultLogLevel=WARN spring-boot:run -Dspring-boot.run.profiles=desktop"
# #end tell'


if [ "$1" = "mvn" ]
then 
    runmvn
elif [ "$1" = "java" ]
then 
    runjava
else
    if [ "$1" != "" ] 
    then
        echo $1 "is not a valid parameter"
    fi
    echo "Do you want to run using java or mvn?"
fi
