### Introduction

These scripts will make it easier to keep your e1p source code up to date, build all the applications with a single command, and run multiple applications simultaneously with less resource use.

### Running it all 
You can run the three commands below in series, go make a cup of tea, and when you come back the the applications will be running with the most recent code.

`./e1p-pull.sh ; ./e1p-build.sh ; ./e1p-run.sh` 

## Instructions


### 0. The basepath
Each of the scripts has a `basepath` variable that you need to set to the directory where you store your code. 
These scripts assume that the source code for all e1p applications are located in the same directory.

### 1. e1p-pull.sh
Pulls changes from the remote repository for each of the e1p applications.

### 2. e1p-build.sh
This builds all seven of the projects sequentially.

- e1p-comparative-rater
- sapi-e1p-auto-offers
- sapi-e1p-home-offers
- sapi-e1p-transfer
- sapi-inquiry
- sapi-offers
- sapi-transfer
### 3. e1p-run.sh (mvn and java -jar)
Starts six of the seven e1p applications in simultaneously in consoles, you don't need six instances of IntelliJ. 

You can choose between using `mnv` and `java` to run the application by editing the script and commenting out the appropriate section.

- sapi-e1p-auto-offers
- sapi-e1p-home-offers
- sapi-e1p-transfer
- sapi-inquiry
- sapi-offers
- sapi-transfer

e1p-comparative-rater is not started due to problems with the `parent/pom.xml`. 

### 4. e1p-change-env.sh
Edits `application-local.desktop` files in e1p-comparative-rater, sapi-offers and sapi-transfer, to point desktop/dev versions of sapi-inquiry, e1p-home-offers, e1p-auto-offers, and sapi-offers.

Example 1 - 

`./e1p-change-env.sh sapiinquiry=desktop sapioffers=dev e1pautooffers=desktop e1phomeoffers=dev`

Example 2 - 

`./e1p-change-env.sh sapioffers=dev e1phomeoffers=desktop`