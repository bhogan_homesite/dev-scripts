#!/bin/bash

basepath=$P20_HOME
if [ ! -d "$basepath" ]; then
    echo "$basepath does not exist. Did you set the P20_HOME environment variable correctly?" 
    exit 1
fi

applications=("sapi-e1p-auto-offers sapi-e1p-home-offers sapi-e1p-transfer sapi-inquiry sapi-offers sapi-transfer e1p-comparative-rater")
#applications=("sapi-e1p-auto-offers sapi-e1p-home-offers")

buildResults=()

for application in ${applications[@]}; do
    buildOutcome="Failure"
    if [ "$application" != "e1p-comparative-rater" ]
    then
        mvn -f $basepath$application/pom.xml clean package -Dmaven.test.skip=true
        if [ $? == 0 ]
        then 
            buildOutcome="Success"
        fi
        buildResults+=("$application, $buildOutcome")

    elif [ "$application" = "e1p-comparative-rater" ]
    then
        mvn -f "$basepath"e1p-comparative-rater/parent-pom/pom.xml clean package -DskipTests
        if [ $? == 0 ]
        then 
            buildOutcome="Success"
        fi
        buildResults+=("$application, $buildOutcome")

    fi
done

echo -e "\n\n\n"
for buildResult in "${buildResults[@]}"
do
    echo $buildResult
done