#!/bin/bash



# running this - 
# you may need to run chmod 755 choice-pull.sh first
# then call it using ./choice-pull.sh 

git_base="git@bitbucket.org:p20/"
basepath=$P20_HOME
if [ ! -d "$basepath" ]; then
    echo "$basepath does not exist. Did you set the P20_HOME environment variable correctly?" 
    exit 1
fi

connect=("sapi-inquiry sapi-offers sapi-aah-auto-offers")
connect_auto_servicing=("sapi-inquiry sapi-offers sapi-aah-auto-offers sapi-servicing sapi-aah-servicing")
connect_home_servicing=("sapi-inquiry sapi-offers sapi-aah-home-offers sapi-servicing sapi-aah-servicing")

homesite_home=("sapi-inquiry sapi-offers sapi-hs-offers")
homesite_home_servicing=("sapi-inquiry sapi-offers sapi-hs-offers sapi-servicing sapi-hs-servicing")

amfam_auto=("sapi-inquiry sapi-offers sapi-amfm-auto-offers")
amfam_auto_servicing=("sapi-inquiry sapi-offers sapi-amfm-auto-offers sapi-servicing sapi-af-servicing")
amfam_servicing=("sapi-servicing sapi-af-servicing")
aah_servicing=("sapi-servicing sapi-aah-servicing")

function pull() {
    applications="${1}"
    echo $applications
    for application in ${applications[@]}; do
        echo $git_base$application
        if [ ! -d "$basepath$application" ] ; then
            echo "Cloning $application"
            git clone "$git_base$application.git" $basepath$application
        fi
        git -C $basepath$application pull     
    done
}

if [ -n "${!1}" ]
then
    pull "${!1}" 
else 
    echo "Invalid application name"
    echo "Valid applications are: connect | connect_auto_servicing | connect_home_servicing | homesite_home | homesite_home_servicing | amfam_auto | amfam_auto_servicing | amfam_servicing | aah_servicing"
fi

