### Introduction

These scripts will make it easier to keep your choice source code up to date, build all the applications with a single command, and run multiple applications simultaneously with less resource use.

### Running it all 
You can run the three commands below in series, go make a cup of tea, and when you come back the the applications will be running with the most recent code.

`./choice-pull.sh application_name ; ./choice-build.sh application_name ; ./choice-run.sh application_name` 

## Instructions

### 0. The basepath
Each of the scripts has a `basepath` variable that you need to set to the directory where you store your code. 
These scripts assume that the source code for all choice applications are located in the same directory.

### 1. choice-pull.sh application_name
Pulls/clones a suites of applications: connect | connect_auto_servicing | connect_home_servicing | homesite_home | homesite_home_servicing | amfam_auto | amfam_auto_servicing | amfam_servicing | aah_servicing.

### 2. choice-build.sh application_name
Builds a suite of application sequentially: connect | connect_auto_servicing | connect_home_servicing | homesite_home | homesite_home_servicing | amfam_auto | amfam_auto_servicing | amfam_servicing | aah_servicing. 

### 3. choice-run.sh application_name
Start a suite of applications in parallel: connect | connect_auto_servicing | connect_home_servicing | homesite_home | homesite_home_servicing | amfam_auto | amfam_auto_servicing | amfam_servicing | aah_servicing. 

### 4. choice-change-env.sh
NOT READY YET. We need to standardize the server.port values used in the applications.