#!/bin/bash

# this assumes you have all the projects in the same directory, and you will have to change the basepath variable

# running this - 
# you may need to run chmod 755 choice-build.sh first
# then call it using ./choice-build.sh connect | connect_auto_servicing | connect_home_servicing | homesite_home | homesite_home_servicing | amfam_auto | amfam_auto_servicing | amfam_servicing | aah_servicing 

basepath=$P20_HOME
if [ ! -d "$basepath" ]; then
    echo "$basepath does not exist. Did you set the P20_HOME environment variable correctly?" 
    exit 1
fi

connect=("sapi-inquiry sapi-offers sapi-aah-auto-offers")
connect_auto_servicing=("sapi-inquiry sapi-offers sapi-aah-auto-offers sapi-servicing sapi-aah-servicing")
connect_home_servicing=("sapi-inquiry sapi-offers sapi-aah-home-offers sapi-servicing sapi-aah-servicing")

homesite_home=("sapi-inquiry sapi-offers sapi-hs-offers")
homesite_home_servicing=("sapi-inquiry sapi-offers sapi-hs-offers sapi-servicing sapi-hs-servicing")

amfam_auto=("sapi-inquiry sapi-offers sapi-amfm-auto-offers")
amfam_auto_servicing=("sapi-inquiry sapi-offers sapi-amfm-auto-offers sapi-servicing sapi-af-servicing")
amfam_servicing=("sapi-servicing sapi-af-servicing")
aah_servicing=("sapi-servicing sapi-aah-servicing")


function build() {
    applications="${1}"
    for application in ${applications[@]}; do
        mvn -f $basepath$application/pom.xml clean package -Dmaven.test.skip=true     
    done
}

if [ -n "${!1}" ]
then
    build "${!1}" 
else 
    echo "Invalid application name"
    echo "Valid applications are: connect | connect_auto_servicing | connect_home_servicing | homesite_home | homesite_home_servicing | amfam_auto | amfam_auto_servicing | amfam_servicing | aah_servicing"
fi
