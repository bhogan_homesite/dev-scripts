#!/bin/bash

# THIS IS NOT READY FOR CHOCIE APPLICATIONS YET. We need to standardize the server.port values used. 

# applicationPaths=("sapi-offers sapi-servicing sapi-inquiry")

# basepath=$P20_HOME
# if [ ! -d "$basepath" ]; then
#     echo "$basepath does not exist. Did you set the P20_HOME environment variable correctly?" 
#     exit 1
# fi

# pathSuffix="/src/main/resources/application-local.properties"

# http="http:\/\/"
# devserver="qa-enterpriseservices.homesitep2.com"

# sapiaahautooffers_desktop="localhost" 
# sapiaahautooffers_dev=$devserver
# sapiaahautooffers_hosts=(" ")

# sapiaahhomeoffers_desktop="" 
# sapiaahhomeoffers_dev=$devserver
# sapiaahhomeoffers_hosts=("ameriprise.auto.offer.host ")


# sapiamfmautooffers_desktop="" 
# sapiamfmautooffers_dev=$devserver
# sapiamfmautooffers_hosts=(" ")

# sapiinquiry_desktop="localhost:8086"
# sapiinquiry_dev=$devserver
# sapiinquiry_hosts=("sapi.inquiry.api.host  inquiry.host")

# sapioffers_desktop="localhost:8088"
# sapioffers_dev=$devserver
# sapioffers_hosts="sapi.offers.api.host"

# e1phomeoffers_desktop="localhost:8082"
# e1phomeoffers_dev=$devserver
# e1phomeoffers_hosts="e1p.home.offers.host"

# e1pautooffers_desktop="localhost:8092"
# e1pautooffers_dev=$devserver
# e1pautooffers_hosts="e1p.auto.offers.host"

# backupConfigFile() {
#     if [[ ! -f $1.orig ]]; then
#         echo "Backing up $1"
#         cp $1 $1.orig
#     fi
# }

# updateConfigFiles () {
#     app_env=$1_$2
#     app_hosts=$1_hosts

#     for path in ${applicationPaths[@]}; do # loop over all the applications that need their configs updated
#         backupConfigFile $basepath$path$pathSuffix

#         for appHost in ${!app_hosts} ; do # the application-desktop.properties files don't refer to hosts in the same way, some call inquiries inquiry.host, other call it sapi.inquiry.api.host 
#             sed  -i "" "s/$appHost.*/$appHost=$http${!app_env}/" $basepath$path$pathSuffix 
#         done
#     done
# }

# for ARGUMENT in "$@"
# do
#     app=$(echo $ARGUMENT | cut -f1 -d=)
#     env=$(echo $ARGUMENT | cut -f2 -d=)   

#     case "$app" in
#             sapiinquiry | e1phomeoffers | e1pautooffers | sapioffers)      
#                 updateConfigFiles $app $env
#                 ;;
#             *) 
#             echo "$app not recognized" 
#             ;;   
#     esac    
# done