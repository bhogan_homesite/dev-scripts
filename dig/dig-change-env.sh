#!/bin/bash

# This script lets you switch between local and dev hosts for each of this services -  sapiinquiry, sapihsoffers.
# Usage ./dig-change-env.sh sapihsoffers=dev sapiinquiry=local
# or ./dig-change-env.sh sapihsoffers=dev sapiinquiry=dev
# the script currently updates applications listed in the applicationPaths variable.

applicationPaths=("sapi-offers")

basepath=$P20_HOME
if [ ! -d "$basepath" ]; then
    echo "$basepath does not exist. Did you set the P20_HOME environment variable correctly?" 
    exit 1
fi

pathSuffix="/src/main/resources/application-local.properties"

http="http:\/\/"
devserver="qa-enterpriseservices.homesitep2.com"

sapihsoffers_local="localhost:8080"
sapihsoffers_dev=$devserver
sapihsoffers_hosts=("hsoffers.host")

sapiinquiry_local="localhost:8086"
sapiinquiry_dev=$devserver
sapiinquiry_hosts=("sapi.inquiry.api.host inquiry.host")


backupConfigFile() {
    if [[ ! -f $1.orig ]]; then
        echo "Backing up $1"
        cp $1 $1.orig
    fi
}

updateConfigFiles () {
    app_env=$1_$2
    app_hosts=$1_hosts

    for path in ${applicationPaths[@]}; do # loop over all the applications that need their configs updated
        backupConfigFile $basepath$path$pathSuffix

        for appHost in ${!app_hosts} ; do # the application-desktop.properties files don't refer to hosts in the same way, some call inquiries inquiry.host, other call it sapi.inquiry.api.host 
            sed  -i "" "s/$appHost.*/$appHost=$http${!app_env}/" $basepath$path$pathSuffix 
            #echo "s/$appHost.*/$appHost=$http${!app_env}/" $basepath$path$pathSuffix 
        done
    done
}

for ARGUMENT in "$@"
do
    app=$(echo $ARGUMENT | cut -f1 -d=)
    env=$(echo $ARGUMENT | cut -f2 -d=)   

    case "$app" in
            sapihsoffers | sapiinquiry)      
                updateConfigFiles $app $env
                ;;
            *) 
            echo "$app not recognized" 
            ;;   
    esac    
done