#!/bin/bash

git_base="git@bitbucket.org:p20/"
basepath=$P20_HOME
if [ ! -d "$basepath" ]; then
    echo "$basepath does not exist. Did you set the P20_HOME environment variable correctly?" 
    exit 1
fi

dig=("sapi-inquiry sapi-offers sapi-hs-offers sapi-hs-servicing sapi-servicing")

function pull() {
    applications="${1}"
    echo $applications
    for application in ${applications[@]}; do
        echo $git_base$application
        if [ ! -d "$basepath$application" ] ; then
            echo "Cloning $application"
            git clone "$git_base$application.git" $basepath$application
        fi
        git -C $basepath$application pull     
    done
}

pull "$dig"