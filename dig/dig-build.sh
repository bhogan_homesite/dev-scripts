#!/bin/bash

basepath=$P20_HOME
if [ ! -d "$basepath" ]; then
    echo "$basepath does not exist. Did you set the P20_HOME environment variable correctly?" 
    exit 1
fi
dig=("sapi-inquiry sapi-offers sapi-hs-offers sapi-hs-servicing sapi-servicing")

function build() {
    applications="${1}"
    for application in ${applications[@]}; do
        mvn -f $basepath$application/pom.xml clean package -Dmaven.test.skip=true     
    done
}

build "$dig" 