### Introduction

These scripts will make it easier to keep your Dig source code up to date, build all the applications with a single command, and run multiple applications simultaneously with less resource use.

### Running it all 
You can run the three commands below in series, go make a cup of tea, and when you come back the the applications will be running with the most recent code.

`./dig-pull.sh  ; ./dig-build.sh  ; ./dig-run.sh ` 

## Instructions

### 0. The basepath
Each of the scripts has a `basepath` variable that you need to set to the directory where you store your code. 
These scripts assume that the source code for all choice applications are located in the same directory.

### 1. dig-pull.sh 
Pulls/clones the Dig projects.

### 2. dig-build.sh 
Builds the Dig projects sequentially.

### 3. dig-run.sh 
Starts the Dig projects in parallel.

### 4. dig-change-env.sh
Edits `application-local.properties` file in sapi-offers to point local/dev versions of sapi-hs-offers and sapi-inquiry.

Example 1 - 

`./dig-change-env.sh sapihsoffers=dev sapiinquiry=local`

Example 2 -

`./dig-change-env.sh sapihsoffers=dev sapiinquiry=dev`
